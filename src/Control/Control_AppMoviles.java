
package Control;

import java.awt.event.ActionListener;
import Modelo.Modelo_AppMoviles;
import Vista.Vista_AppMoviles; 
import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Control_AppMoviles implements ActionListener{
    Vista_AppMoviles view = new Vista_AppMoviles(); 
    Modelo_AppMoviles model =  new Modelo_AppMoviles(); 
    
    public Control_AppMoviles(Vista_AppMoviles vista, Modelo_AppMoviles mod)
    {
        this.view = vista; 
        this.model = mod; 
        
        this.view.aac.addActionListener(this); 
        this.view.comp.addActionListener(this); 
        this.view.can.addActionListener(this);
        
    }
    
    public void iniciarMoviles()
    {
        view.setSize(600,250);
        view.setTitle("Aplicaciones Disponibles");
        view.setLocationRelativeTo(null);
        view.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        view.setVisible(true); 
    }
    public void actionPerformed(ActionEvent evt)
    {
        if(view.can == evt.getSource())
        {
            view.can.addActionListener(e-> view.dispose()); 
        }
         if (view.aac == evt.getSource())
       {
          
           view.articulo.addElement("Articulo: " +  " : -->> " +  "  " + view.all[view.products.getSelectedIndex()] );
           view.venta.addElement("Articulo: " +  " : -->> " +  "  " + view.all[view.products.getSelectedIndex()] );
           view.products.setSelectedIndex(0); 
           view.comprado.setModel(view.articulo);
           view.content_venta.setModel(view.venta);
           JOptionPane.showMessageDialog(null, "Agregado Correctamente"); 
       
       }
       if(view.comp == evt.getSource())
       {
        if(view.dinero.isSelected())
           {
              view.venta.addElement("Su forma de pago seleccionada: " + view.dinero.getText());
              
              view.content_venta.setModel(view.venta);
           }
        if (view.tarjeta.isSelected())
        {
            view.venta.addElement("Su forma de pago seleccionada: " + view.tarjeta.getText());
           
            view.content_venta.setModel(view.venta);
        }
       JOptionPane.showMessageDialog(null, "Venta Exitosa!");
       view.articulo.removeAllElements();
       view.setVisible(false);
       }
    }
}
