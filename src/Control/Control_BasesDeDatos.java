
package Control;
import Modelo.Modelo_BasesDeDatos;
import Vista.Vista_BasesDeDatos;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Control_BasesDeDatos implements ActionListener{
    public Modelo_BasesDeDatos model = new Modelo_BasesDeDatos();
    public Vista_BasesDeDatos view = new Vista_BasesDeDatos();
    
    public Control_BasesDeDatos(Modelo_BasesDeDatos mod, Vista_BasesDeDatos vis){
        
        this.view = vis;
        this.model = mod;
        
        this.view.aac.addActionListener(this);
        this.view.can.addActionListener(this);
        this.view.comp.addActionListener(this);
    }
    
    public void iniciarBase(){
        view.setSize(600,250);
        view.setTitle("Data Base Disponibles");
        view.setLocationRelativeTo(null);
        view.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        view.setVisible(true);
    }
    
    public void actionPerformed(ActionEvent evt){
        
        if(view.can == evt.getSource())
        {
            view.can.addActionListener(e -> view.dispose());
        }
        if (view.aac == evt.getSource())
       {
          
           view.articulo.addElement("Articulo: " +  " : -->> " +  "  " + view.all[view.products.getSelectedIndex()] );
           view.venta.addElement("Articulo: " +  " : -->> " +  "  " + view.all[view.products.getSelectedIndex()] );
           view.products.setSelectedIndex(0); 
           view.comprado.setModel(view.articulo);
           view.content_venta.setModel(view.venta);
           JOptionPane.showMessageDialog(null, "Agregado Correctamente"); 
       
       }
       if(view.comp == evt.getSource())
       {
        if(view.efectivo.isSelected())
           {
              view.venta.addElement("Su forma de pago seleccionada: " + view.efectivo.getText());
           
              view.content_venta.setModel(view.venta);
           }
        if (view.tarjeta.isSelected())
        {
            view.venta.addElement("Su forma de pago seleccionada: " + view.tarjeta.getText());
          
            view.content_venta.setModel(view.venta);
        }
       JOptionPane.showMessageDialog(null, "Venta Exitosa!");
       view.articulo.removeAllElements();
       view.setVisible(false);
       }
    }
        
    
}
