/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;
import java.awt.event.ActionListener; 
import Vista.Vista_ReporteVenta;
import Modelo.Modelo_ReporteVenta;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import Vista.Vista_AppEscritorio;
/**
 *
 * @author Alexis
 */
public class Control_ReporteVenta implements ActionListener{
    public Vista_ReporteVenta view;
    public Modelo_ReporteVenta model;
    public Vista_AppEscritorio appes;
    
    public Control_ReporteVenta(Vista_ReporteVenta view,Modelo_ReporteVenta model ){
        this.view = view;
        this.model = model; 
        
        this.view.salir.addActionListener(this);
        
    }
    
    public void abrirReporte(){
        view.setTitle("Reporte de Ventas");
        view.setSize(600,250);
        view.setLocationRelativeTo(null);
        view.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        view.setVisible(true);
        
    }
    
    public void actionPerformed(ActionEvent evt ){
        if (view.salir == evt.getSource())
        {
            view.setVisible(false); 
        }
        
        
    }
    
}
