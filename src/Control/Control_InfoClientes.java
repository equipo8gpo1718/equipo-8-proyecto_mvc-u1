package Control;

import Modelo.Modelo_InfoClientes;
import Vista.Vista_InfoClientes;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import Vista.prueba; 
import javax.swing.JOptionPane;
/**
 *
 * @author Donato
 */
public class Control_InfoClientes implements ActionListener {
    private Modelo_InfoClientes registro;
    private Vista_InfoClientes vistaDeClientes;
    

    public Control_InfoClientes(Modelo_InfoClientes registro, Vista_InfoClientes vistaDeClientes) {
        this.registro = registro;
        this.vistaDeClientes = vistaDeClientes;
        
        this.vistaDeClientes.bs1.addActionListener(this);
        this.vistaDeClientes.bs2.addActionListener(this);
        this.vistaDeClientes.bo1.addActionListener(this);
    }
        public void iniciarVistaInfoClientes(){
    
         vistaDeClientes.setSize(500,500);
         vistaDeClientes.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
         vistaDeClientes.setTitle("Registra nuevo cliente");
         vistaDeClientes.setLocationRelativeTo(null);
         vistaDeClientes.setVisible(true);
         
}
        
        public void actionPerformed(ActionEvent evento){
            if (vistaDeClientes.bs1 == evento.getSource()) { 
                 vistaDeClientes.listaClients.addElement(vistaDeClientes.tf1.getText() + " "+ 
                         vistaDeClientes.tf2.getText() + " "+ 
                         vistaDeClientes.tf3.getText() + " "+
                         vistaDeClientes.tf4.getText() +" " +vistaDeClientes.lista[vistaDeClientes.cb1.getSelectedIndex()] );
                         
        
                vistaDeClientes.tf1.setText(" ");
                vistaDeClientes.tf2.setText(" ");
                vistaDeClientes.tf3.setText(" ");
                vistaDeClientes.tf4.setText(" ");
                vistaDeClientes.cb1.setSelectedIndex(0);
                vistaDeClientes.listaClientes.setModel(vistaDeClientes.listaClients);
                
                JOptionPane.showMessageDialog(null, "Registro Exitoso");
                
            }
            if (vistaDeClientes.bs2 == evento.getSource())
            {
                vistaDeClientes.setVisible(false); 
            }
            
            if (vistaDeClientes.bo1 == evento.getSource())
            {
                prueba pp = new prueba(); 
                pp.marco();
                 pp.setVisible(true);   
                }
            }
        }

        
    
    

