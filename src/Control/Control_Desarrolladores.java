/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import java.awt.event.ActionListener;
import Modelo.Modelo_Desarrolladores;
import Vista.Vista_Desarrolladores;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;
/**
 *
 * @author Alexis
 */
public class Control_Desarrolladores implements ActionListener{
    public Modelo_Desarrolladores model;
    public Vista_Desarrolladores view; 
    
    public Control_Desarrolladores(Vista_Desarrolladores view, Modelo_Desarrolladores model)
    {
        this.view = view;
        this.model = model; 
        
        this.view.exit.addActionListener(this); 
    }
    
    public void EmpezarDesarrolladores()
    {
        view.setTitle("Desarrolladores del Sistema");
        view.pack();
        view.setLocationRelativeTo(null);
        view.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        view.setVisible(true);
    }
    
    public void actionPerformed(ActionEvent evt)
    {
        if (view.exit == evt.getSource())
        {
            view.setVisible(false);
        }
    }
    
}
