
package Control;

import java.awt.event.ActionListener;
import Modelo.Modelo_AppEscritorio;
import Vista.Vista_AppEscritorio;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Control_AppEscritorio implements ActionListener{
    public Modelo_AppEscritorio model;
    public Vista_AppEscritorio view; 
    public Control_AppEscritorio(Modelo_AppEscritorio mod, Vista_AppEscritorio vis){
        this.model = mod;
        this.view = vis; 
        
        this.view.aac.addActionListener(this);
        this.view.can.addActionListener(this);
        this.view.comp.addActionListener(this);
       
    }
    
    public void iniciarVistaEscritorio()
    {
        view.setTitle("Programas Disponibles");
        view.setSize(600,250);
        view.setLocationRelativeTo(null);
        view.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        view.setVisible(true);
        
    }
    
    public void actionPerformed(ActionEvent evt)
    {
        
        
       if(view.can == evt.getSource())
       {
        view.can.addActionListener(e -> view.dispose());
       }
       if (view.aac == evt.getSource())
       {
          
           view.articulo.addElement("Articulo: " +  " : -->> " +  "  " + view.all[view.products.getSelectedIndex()] );
           view.venta.addElement("Articulo: " +  " : -->> " +  "  " + view.all[view.products.getSelectedIndex()] );
           view.products.setSelectedIndex(0); 
           view.comprado.setModel(view.articulo);
           view.content_venta.setModel(view.venta);
           JOptionPane.showMessageDialog(null, "Agregado Correctamente"); 
       
       }
       if(view.comp == evt.getSource())
       {
        if(view.efectivo.isSelected())
           {
              view.venta.addElement("Su forma de pago seleccionada: " + view.efectivo.getText());
           
              view.content_venta.setModel(view.venta);
           }
        if (view.tarjeta.isSelected())
        {
            view.venta.addElement("Su forma de pago seleccionada: " + view.tarjeta.getText());
          
            view.content_venta.setModel(view.venta);
        }
       JOptionPane.showMessageDialog(null, "Venta Exitosa!");
       view.articulo.removeAllElements();
       view.setVisible(false);
       }
    }
    
}
