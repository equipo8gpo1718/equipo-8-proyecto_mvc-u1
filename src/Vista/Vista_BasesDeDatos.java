package Vista;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.DefaultListModel;


import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;


public class Vista_BasesDeDatos extends JFrame{
    public JPanel totalPanel, northPanel, centerPanel, southPanel;
    public JLabel select, forma_pago;
    public String [] all;
    public JComboBox products;
    public JButton aac, comp, can;
    public GridBagConstraints gbc1;
    public JCheckBox efectivo,tarjeta; 
    public DefaultListModel articulo,venta; 
    public JList comprado,content_venta; 
    
    public Vista_BasesDeDatos(){
        getContentPane().setLayout(new BorderLayout());
        
        totalPanel = new JPanel();
        totalPanel.setLayout(new BorderLayout());
        
         northPanel = new JPanel();
        northPanel.setLayout(new GridBagLayout());
        northPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Registre su Venta"));
        
        centerPanel = new JPanel(); 
        centerPanel.setLayout(new GridBagLayout()); 
        centerPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        
        gbc1 = new GridBagConstraints(); 
        
        southPanel = new JPanel();
        southPanel.setLayout(new FlowLayout());
        
        select = new JLabel("Seleccione una opcion: ");
        forma_pago = new JLabel("Elija la Forma de Pago");
        
        all = new String [] {"Seleccione una opción", "Phoenix", "Oracle", "MySQL", "SQLServer"};
        products = new JComboBox(all); 
        
        aac = new JButton("Agregar al carrito");
        comp = new JButton("Comprar");
        can = new JButton("Cancelar");
        
        comprado = new JList();
        articulo = new DefaultListModel(); 
        
        venta = new DefaultListModel();
        content_venta = new JList();
        
        efectivo = new JCheckBox("Efectivo");
        tarjeta = new JCheckBox("Tarjeta de Credito");
        //Posición del GridBagConstraints
        gbc1.gridx =0;
        gbc1.gridy = 0;
        gbc1.anchor = GridBagConstraints.WEST;
        
        //Agregado del Label y el ComboBox, JButton, JCheckBox al Panel Central
        northPanel.add(select,gbc1); 
        gbc1.gridx=1;       
        northPanel.add(products,gbc1);
        gbc1.gridx=3;
        gbc1.gridy=0;
        northPanel.add(aac,gbc1);
        gbc1.gridy=2;
        gbc1.gridx=0;
        northPanel.add(forma_pago,gbc1);
        gbc1.gridy = 4;
        gbc1.gridx=1;
        northPanel.add(efectivo,gbc1);
        gbc1.gridy = 4;
         gbc1.gridx = 2;
        northPanel.add(tarjeta,gbc1);
        gbc1.gridx = 1;
        gbc1.gridy = 6;
        
        
        gbc1.gridx = 0;
        gbc1.gridy = 0;
        centerPanel.add(comprado,gbc1);
        
        //
        
        
        //Agregado de los botones Comprar y Cancelar al panel Sur
        southPanel.add(comp);
        southPanel.add(can);
        
        //Acomodar los paneles en su lugar del BorderLayout y agregar los al panel total
       totalPanel.add(northPanel, BorderLayout.NORTH);
        totalPanel.add(centerPanel, BorderLayout.CENTER); 
        totalPanel.add(southPanel, BorderLayout.SOUTH); 
        this.add(totalPanel);
    }
    
    
}
