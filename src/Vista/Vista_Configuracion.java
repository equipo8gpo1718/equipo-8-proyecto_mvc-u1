/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Alexis
 */
public class Vista_Configuracion extends JFrame{
    public JLabel config; 
    public JPanel pnl, pnl2, pnl3; 
    public GridBagConstraints gbc1;
    public JButton azul,naranja;
    public Vista_Configuracion(){
        getContentPane().setLayout(new BorderLayout());
        
        config = new JLabel("Configuracion del menu \n cambiar color de fondo"); 
        pnl = new JPanel(new BorderLayout());
        pnl2 = new JPanel(new FlowLayout());
        
         pnl3= new JPanel();
        pnl3.setLayout(new GridBagLayout());
         gbc1 = new GridBagConstraints(); 
         
         azul = new JButton("azul"); 
         naranja = new JButton("naranja"); 
        
         gbc1.gridx =0;
        gbc1.gridy = 0;
        gbc1.anchor = GridBagConstraints.WEST;
        
        pnl3.add(azul,gbc1); 
        gbc1.gridy =1;
        pnl3.add(naranja, gbc1);
        gbc1.gridx = 2; 
        
        
        pnl3.add(azul,naranja); 
        pnl2.add(config, FlowLayout.LEFT);
        pnl.add(pnl2, BorderLayout.CENTER);
        pnl.add(pnl3, BorderLayout.SOUTH); 
    }
}
