/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;


import java.awt.BorderLayout;
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

/**
 *
 * @author Alexis
 */
public class Vista_Desarrolladores extends JFrame{
    public JPanel pnl1, panelCentro, panelNorte; 
    public JTextArea txa1; 
    public JButton exit;
    
    public Vista_Desarrolladores(){
       getContentPane().setLayout(new BorderLayout());
        
        pnl1 = new JPanel();
        FlowLayout fl1 = new FlowLayout(FlowLayout.CENTER); 
        panelCentro = new JPanel();
        panelNorte = new JPanel(new BorderLayout()); 
        panelNorte.setBorder(javax.swing.BorderFactory.createTitledBorder("Desarrolladores del sistema"));
        
       
        txa1 = new JTextArea("Ana Laura Jacome Gonzalez "
                                    + "\nAlexis Salazar Viveros "
                                    + "\nDonato Garcia Romero");
        txa1.setEditable(false); 
        
        exit = new JButton("Salir");
        
        
       
        panelCentro.add(exit);
    panelNorte.setLayout(fl1);   
    panelNorte.add(txa1);
    pnl1.add(panelNorte);
    pnl1.add(panelCentro);
    this.add(pnl1);
}
}
