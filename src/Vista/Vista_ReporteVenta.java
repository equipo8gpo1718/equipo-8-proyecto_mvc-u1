/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
/**
 *
 * @author Alexis
 */
public class Vista_ReporteVenta extends JFrame{ 
    Vista_AppMoviles movil = new Vista_AppMoviles();
    Vista_AppEscritorio desk = new Vista_AppEscritorio();
   Vista_BasesDeDatos bd = new Vista_BasesDeDatos();
    
    
    public JPanel norte,centro,sur,granPanel; 
    public GridBagConstraints gbc1; 
    public JButton salir;  
    public JList Content_pane;
    public DefaultListModel vender;
    
    public  Vista_ReporteVenta(){
        
        getContentPane().setLayout(new BorderLayout());
        
        granPanel = new JPanel(new BorderLayout());
        
        norte = new JPanel();
        norte.setLayout(new GridBagLayout());
        norte.setBorder(javax.swing.BorderFactory.createTitledBorder("Ventas Registradas de Bases de Datos"));
        
        centro = new JPanel();
        centro.setLayout(new GridBagLayout());
        centro.setBorder(javax.swing.BorderFactory.createTitledBorder("Ventas Registradas de Aplicaciones Moviles"));
        
        sur = new JPanel();
        sur.setLayout(new GridBagLayout()); 
        sur.setBorder(javax.swing.BorderFactory.createTitledBorder("Ventas Registradas de Programas de Escritorio"));
        
        gbc1 = new GridBagConstraints();
        Content_pane = new JList();
        vender = new DefaultListModel();
        salir = new JButton("Salir");
        
//Jlist al panel norte 
        gbc1.gridx = 0;
        gbc1.gridy = 0;
        norte.add(bd.content_venta,gbc1);
       
       
        
        //Jlist al panel centro
        gbc1.gridx = 0;
        gbc1.gridy = 0;
        centro.add(movil.content_venta,gbc1); 
        
        //Jlist al panel sur + un JButton para salir del Frame 
        gbc1.gridx = 0;
        gbc1.gridy = 0;
       //sur.add(Content_pane,gbc1);
        sur.add(desk.content_venta,gbc1); 
        gbc1.gridy = 2; 
        sur.add(salir,gbc1);
        
        
        granPanel.add(norte, BorderLayout.NORTH);
        granPanel.add(centro, BorderLayout.CENTER);
        granPanel.add(sur, BorderLayout.SOUTH);
        this.add(granPanel);
    }
    
    
}
