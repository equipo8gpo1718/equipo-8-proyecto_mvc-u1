package Vista;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Donato
 */
public class Vista_InfoClientes extends JFrame{
    public JPanel panelMayor, panelNorte, panelCentro, panelSur, panelEste, panelOeste;
    public JLabel lblTitulo, lbl1, lbl2, lbl3, lbl4, lbl5;
    public JTextField tf1, tf2, tf3, tf4;
    public String [] lista;
    public JComboBox cb1;
    public JButton bs1, bs2, bo1;
    public GridBagConstraints gbc1;
    public DefaultListModel listaClients, t;
    public JList listaClientes,tester;
    
    public Vista_InfoClientes() {
        getContentPane().setLayout(new BorderLayout());
        
        panelMayor = new JPanel();
        panelMayor.setLayout(new BorderLayout());
        
       // panelNorte = new JPanel();
        //panelNorte.setLayout(new FlowLayout());
        
        panelCentro = new JPanel();
        panelCentro.setLayout(new GridBagLayout());
        panelCentro.setBorder(javax.swing.BorderFactory.createTitledBorder("Rellene los campos para agregar un nuevo cliente"));
        gbc1 = new GridBagConstraints();
        
        panelSur = new JPanel();
        panelSur.setLayout(new FlowLayout());
        
        panelEste = new JPanel();
        panelEste.setLayout(new GridLayout());
        
        panelOeste = new JPanel();
        //panelOeste.setLayout(new GridLayout();
        
        //Para el panel Norte:
        //lblTitulo = new JLabel("Rellene los campos para agregar un nuevo cliente");
        
        //Para el Panel Centro:
        lbl1 = new JLabel("Nombre:");
        tf1 = new JTextField(25);
        lbl2 = new JLabel("Dirección:");
        tf2 = new JTextField(25);
        lbl3 = new JLabel("Telefono:");
        tf3 = new JTextField(25);
        lbl4 = new JLabel("Ciudad:");
        tf4 = new JTextField(25);
        
        lista = new String [] {"Seleccione un estado", "Aguascalientes", "Baja California Norte", "Baja California Sur", "Campeche", "Coahuila", "Chiapas",
            "Chihuahua", "Durango", "Estado de Mexico", "Guanajuato", "Guerrero", "Hidalgo", "Jalisco", "Michoacan", "Morelos", "Mexico, D.F.", "Nayarit",
            "Nuevo Leon", "Oaxaca", "Puebla", "Queretaro", "Quintana Roo", "San Luis Potosi", "Sinaloa", "Sonora", "Tabasco", "Tamaulipas", "Tlaxcala", "Veracruz",
        "Yucatan", "Zacatecas"};
        lbl5 = new JLabel("Estado:");
        cb1 = new JComboBox(lista);
        listaClients = new DefaultListModel();
        listaClientes = new JList();
        tester = new JList();
        t = new DefaultListModel();
        
        t.addElement("la la");
        tester.setModel(t);
        
        
        
        //Para el panel Sur:
        bs1 = new JButton("Registrar");
        bs2 = new JButton("Cancelar");
        
        //Para el panel Oeste:
        bo1 = new JButton("Lista de Clientes");
        
        
        //Ahora agregar los componentes a su respectivo panel
       // panelNorte.add(lblTitulo);
        
        gbc1.gridx = 0;
        gbc1.gridy = 0;
        
        gbc1.anchor = GridBagConstraints.WEST;
        panelCentro.add(lbl1, gbc1);
        gbc1.gridy = 1;
        gbc1.gridwidth = 5;
        panelCentro.add(tf1,gbc1);
        gbc1.gridwidth = 1;
        gbc1.gridy = 2;
        panelCentro.add(lbl2,gbc1);
        gbc1.gridy = 3;
        gbc1.gridwidth = 8;
        panelCentro.add(tf2,gbc1);
        gbc1.gridwidth = 1;
        gbc1.gridy = 4;
        panelCentro.add(lbl3,gbc1);
        gbc1.gridy = 5;
        gbc1.gridwidth = 5;
        panelCentro.add(tf3,gbc1);
        gbc1.gridwidth = 1;
        gbc1.gridy = 6;
        panelCentro.add(lbl4,gbc1);
        gbc1.gridwidth = 5;
        gbc1.gridy = 7;
        panelCentro.add(tf4,gbc1);
        gbc1.gridwidth = 1;
        gbc1.gridy = 8;
        panelCentro.add(lbl5,gbc1);
        gbc1.gridwidth = 2;
        gbc1.gridy = 9;
        panelCentro.add(cb1, gbc1);
      
        // panelSur.add(listaClientes);
        
        panelSur.add(bs1);
        panelSur.add(bs2);
        
        
        panelOeste.add(bo1);
       
        
      //  panelMayor.add(panelNorte, BorderLayout.NORTH);
        panelMayor.add(panelCentro, BorderLayout.CENTER);
        panelMayor.add(panelSur, BorderLayout.SOUTH);  
        panelMayor.add(panelEste, BorderLayout.EAST);
        panelMayor.add(panelOeste, BorderLayout.WEST);
        this.add(panelMayor);
     
    }
    
}
